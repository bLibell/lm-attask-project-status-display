USE [AtTaskScheduleManager]
GO

/****** Object:  Table [dbo].[AllocatedHours]    Script Date: 06/03/2013 17:06:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ScheduleItemTable](
	[Id] [uniqueidentifier] NOT NULL,
	[ProjectId] [uniqueidentifier] NOT NULL FOREIGN KEY REFERENCES ProjectTable(Id),
	[TaskId] [uniqueidentifier] NOT NULL FOREIGN KEY REFERENCES TaskTable(Id),
	[UserId] [uniqueidentifier] NOT NULL FOREIGN KEY REFERENCES UserTable(Id),
	[Hours] [int] NOT NULL,
	[Abbreviation] [varchar](255) NOT NULL,
	[Date] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Index [Create_Date]    Script Date: 02/17/2014 10:18:00 ******/
CREATE CLUSTERED INDEX [Create_Date] ON [dbo].[ScheduleItemTable] 
(
	[CreateDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
 
ALTER TABLE 
	[dbo].[ScheduleItemTable] ADD CONSTRAINT [Schedule_Create_DateTime] DEFAULT GETDATE() FOR [CreateDate]