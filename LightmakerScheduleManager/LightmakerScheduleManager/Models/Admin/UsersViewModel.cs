﻿using LMScheduleManager.Models.DataModels.Interfaces;
using LMScheduleManager.Models.ViewModels;
using LMScheduleManager.Models.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightmakerScheduleManager.Models.Admin
{
    public class UsersViewModel : ViewModel, IUsersViewModel
    {
        public IEnumerable<IUser> Users { get; set; }
        public ITeam Team { get; set; }
    }
}