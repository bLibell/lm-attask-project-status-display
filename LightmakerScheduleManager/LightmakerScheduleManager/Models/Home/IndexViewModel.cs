﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMScheduleManager.Models.ViewModels;
using LMScheduleManager.Models.ViewModels.Interfaces;
using LMScheduleManager.Models.DataModels.Interfaces;

namespace LightmakerScheduleManager.Models.Home
{
    public class IndexViewModel : ViewModel, IIndexViewModel 
    {
        public IEnumerable<IScheduleEntry> ScheduleForWeek { get; set; }
        public DateTime StartDay { get { return DateTime.Today.AddDays(DateTime.Today.DayOfWeek - DayOfWeek.Monday); } }
        public DateTime EndDay { get { return this.StartDay.AddDays(4); } }
    }
}
