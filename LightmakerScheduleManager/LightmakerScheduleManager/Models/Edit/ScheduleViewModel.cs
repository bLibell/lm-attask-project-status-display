﻿using LMScheduleManager.Models.DataModels.Interfaces;
using LMScheduleManager.Models.ViewModels;
using LMScheduleManager.Models.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightmakerScheduleManager.Models.Edit
{
    public class ScheduleViewModel : ViewModel, IScheduleViewModel
    {
        public IEnumerable<ITask> Tasks { get; set; }
        public IEnumerable<IScheduleEntry> WeeklySchedule { get; set; }
        public IUser User { get; set; }
        public DateTime StartDay { get { return DateTime.Today.AddDays(DateTime.Today.DayOfWeek - DayOfWeek.Monday); } }
        public DateTime EndDay { get { return this.StartDay.AddDays(4); } }
    }
}