﻿using LightmakerScheduleManager.Models.Edit;
using LMScheduleManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LightmakerScheduleManager.Controllers
{
    public class EditController : Controller
    {
        private readonly IScheduleEntryRepository _scheduleEntryRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITaskRepository _taskRepository;

        public EditController(IScheduleEntryRepository scheduleEntryRepository, IUserRepository userRepository, ITaskRepository taskRepository)
        {
            _scheduleEntryRepository = scheduleEntryRepository;
            _userRepository = userRepository;
            _taskRepository = taskRepository;
        }
        
        public ActionResult Schedule(string id)
        {
            var model = new ScheduleViewModel();

            model.WeeklySchedule = _scheduleEntryRepository.GetForUserInDateRange(model.StartDay, model.EndDay, Guid.Parse(id));
            model.User = _userRepository.GetById(Guid.Parse(id));
            model.Tasks = _taskRepository.GetAll();

            return View(model);
        }
	}
}