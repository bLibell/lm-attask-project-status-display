﻿using LightmakerScheduleManager.Models.Home;
using LMScheduleManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LightmakerScheduleManager.Controllers
{
    public class HomeController : Controller
    {

        private readonly IScheduleEntryRepository _scheduleRepository;

        public HomeController(IScheduleEntryRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }

        public ActionResult Index()
        {
            var model = new IndexViewModel();

            model.ScheduleForWeek = _scheduleRepository.GetAllInDateRange(model.StartDay, model.EndDay);

            return View(model);
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}