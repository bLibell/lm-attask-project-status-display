﻿using LightmakerScheduleManager.Models.Admin;
using LMScheduleManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LightmakerScheduleManager.Controllers
{
    public class AdminController : Controller
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;

        public AdminController(ITeamRepository teamRepository, IUserRepository userRepository)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public ActionResult Teams()
        {
            var model = new TeamsViewModel();

            model.Teams = _teamRepository.GetAll();

            return View(model);
        }

        public ActionResult Users(string id)
        {
            var model = new UsersViewModel();

            model.Users = _userRepository.GetUsersForTeam(Guid.Parse(id));
            model.Team = _teamRepository.GetById(Guid.Parse(id));

            return View(model);
        }
	}
}