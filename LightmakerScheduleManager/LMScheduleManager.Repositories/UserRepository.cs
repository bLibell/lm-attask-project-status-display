﻿using LMScheduleManager.Models.DataModels;
using LMScheduleManager.Models.DataModels.Interfaces;
using LMScheduleManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using Dapper;

namespace LMScheduleManager.Repositories
{
    public class UserRepository : IUserRepository
    {
        public readonly string _connectionString;

        public UserRepository()
        {
            //_connectionString = ConfigurationManager.ConnectionStrings["ScheduleManagerDb"].ConnectionString;
        }

        public IEnumerable<IUser> GetAll()
        {
            IEnumerable<IUser> users = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        users = cn.Query<IUser>("SELECT u.Id AS Id, u.Name AS Name, t.Id AS TeamId, t.Name AS TeamName " + 
                                                "FROM UserTable u" +
                                                "INNER JOIN TeamTable t" +
                                                "ON u.TeamId = t.Id");
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }

            return users;
        }

        public IUser GetById(Guid id)
        {
            IUser user = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        user = cn.Query<IUser>("SELECT u.Id AS Id, u.Name AS Name, t.Id AS TeamId, t.Name AS TeamName " +
                                                "FROM UserTable u" +
                                                "INNER JOIN TeamTable t" +
                                                "ON u.TeamId = t.Id" +
                                                "WHERE u.Id = '" + id + "'").FirstOrDefault();
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }

            return user;
        }

        public void Insert(IUser item)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        cn.Execute(("INSERT UserTable (Id, Name, TeamId)" +
                                    "VALUES (@Id, @Name, @Team.Id)"), item);
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Insert(IEnumerable<IUser> users)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        cn.Execute(("INSERT UserTable (Id, Name, TeamId)" +
                                    "VALUES (@Id, @Name, @Team.Id)"), users);
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Update(IUser item)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        var updateQuery = string.Format("UPDATE UserTable " +
                                                        "SET Id = '" + item.Id + "', Name = '" + item.Name + "', TeamId = '" + item.Team.Id + "'" +
                                                        "WHERE Id = '" + item.Id + "'");
                        cn.Execute(updateQuery);
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Update(IEnumerable<IUser> users)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        foreach (var item in users)
                        {
                            var updateQuery = string.Format("UPDATE UserTable " +
                                                            "SET Id = '" + item.Id + "', Name = '" + item.Name + "', TeamId = '" + item.Team.Id + "'" +
                                                            "WHERE Id = '" + item.Id + "'");
                            cn.Execute(updateQuery);
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Delete(Guid id)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        cn.Execute(string.Format("DELETE FROM UserTable" +
                                                 "WHERE Id = '{0}'", id));
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public IEnumerable<IUser> GetUsersForTeam(Guid id)
        {
            IEnumerable<IUser> users = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        users = cn.Query<IUser>("SELECT u.Id AS Id, u.Name AS Name, t.Id AS TeamId, t.Name AS TeamName" +
                                                "FROM UserTable u" +
                                                "INNER JOIN TeamTable t" +
                                                "ON u.TeamId = t.Id" +
                                                "WHERE u.TeamId = '" + id + "'");
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
            return users;
        }
    }
}
