﻿using LMScheduleManager.Models.DataModels;
using LMScheduleManager.Models.DataModels.Interfaces;
using LMScheduleManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Repositories
{
    public class ScheduleEntryRepository : IScheduleEntryRepository
    {
        public IEnumerable<IScheduleEntry> GetAll()
        {
            return new List<IScheduleEntry>();
        }

        public IScheduleEntry GetById(Guid id)
        {
            return new ScheduleEntry();
        }

        public void Insert(IScheduleEntry item)
        {

        }

        public void Update(IScheduleEntry item)
        {

        }

        public void Delete(Guid id)
        {

        }

        public IEnumerable<IScheduleEntry> GetAllInDateRange(DateTime startDate, DateTime endDate)
        {
            return new List<IScheduleEntry>();
        }

        public IEnumerable<IScheduleEntry> GetForUserInDateRange(DateTime startDate, DateTime endDate, Guid userId)
        {
            return new List<ScheduleEntry>();
        }
    }
}
