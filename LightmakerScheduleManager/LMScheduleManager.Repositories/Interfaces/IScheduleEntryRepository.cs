﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Repositories.Interfaces
{
    public interface IScheduleEntryRepository : IRepository<IScheduleEntry>
    {
        IEnumerable<IScheduleEntry> GetAllInDateRange(DateTime startDate, DateTime endDate);
        IEnumerable<IScheduleEntry> GetForUserInDateRange(DateTime startDate, DateTime endDate, Guid userId);
    }
}
