﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Repositories.Interfaces
{
    public interface ITaskRepository : IRepository<ITask>
    {
        void Insert(IEnumerable<ITask> users);
        void Update(IEnumerable<ITask> users);
    }
}
