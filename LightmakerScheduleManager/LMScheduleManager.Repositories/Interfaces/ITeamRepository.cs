﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Repositories.Interfaces
{
    public interface ITeamRepository : IRepository<ITeam>
    {
        void Insert(IEnumerable<ITeam> teams);
        void Update(IEnumerable<ITeam> teams);
    }
}
