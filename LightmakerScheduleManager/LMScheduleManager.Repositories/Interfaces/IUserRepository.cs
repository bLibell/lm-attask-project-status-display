﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<IUser>
    {
        void Insert(IEnumerable<IUser> users);
        void Update(IEnumerable<IUser> users);
        IEnumerable<IUser> GetUsersForTeam(Guid id);
    }
}
