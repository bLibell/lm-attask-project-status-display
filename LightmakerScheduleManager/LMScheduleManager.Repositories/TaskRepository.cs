﻿using LMScheduleManager.Models.DataModels.Interfaces;
using LMScheduleManager.Repositories.Interfaces;
using LMScheduleManager.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using Dapper;

namespace LMScheduleManager.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        public readonly string _connectionString;

        public TaskRepository()
        {
            //_connectionString = ConfigurationManager.ConnectionStrings["ScheduleManagerDb"].ConnectionString;
        }

        public IEnumerable<ITask> GetAll()
        {
            IEnumerable<ITask> tasks = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        tasks = cn.Query<ITask>("SELECT t.Id, t.Name, p.Id AS ProjectId, p.Name AS ProjectName, t.TotalHours, t.UsedHours, t.DueDate " +
                                                "FROM TaskTable t" +
                                                "INNER JOIN ProjectTable p" +
                                                "ON t.ProjectId = p.Id");
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }

            return tasks;
        }

        public ITask GetById(Guid id)
        {
            ITask task = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        task = cn.Query<ITask>("SELECT t.Id, t.Name, p.Id AS ProjectId, p.Name AS ProjectName, t.TotalHours, t.UsedHours, t.DueDate " +
                                               "FROM TaskTable t" +
                                               "INNER JOIN ProjectTable p" +
                                               "ON t.ProjectId = p.Id" +
                                               "WHERE t.Id = '" + id + "'").FirstOrDefault();
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }

            return task;
        }

        public void Insert(ITask item)
        {

        }

        public void Insert(IEnumerable<ITask> users)
        {

        }
        
        public void Update(ITask item)
        {

        }

        public void Update(IEnumerable<ITask> users)
        {

        }

        public void Delete(Guid id)
        {

        }
    }
}
