﻿using LMScheduleManager.Models.DataModels;
using LMScheduleManager.Models.DataModels.Interfaces;
using LMScheduleManager.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using Dapper;

namespace LMScheduleManager.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        public readonly string _connectionString;

        public TeamRepository()
        {
            //_connectionString = ConfigurationManager.ConnectionStrings["ScheduleManagerDb"].ConnectionString;
        }
        public IEnumerable<ITeam> GetAll()
        {
            IEnumerable<ITeam> teams = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        teams = cn.Query<ITeam>("SELECT Id, Name FROM TeamTable");
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }

            return teams;
        }

        public ITeam GetById(Guid id)
        {
            ITeam team = null;

            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        team = cn.Query<ITeam>(string.Format("SELECT Id, Name FROM TeamTable" +
                                                             "WHERE Id = '{0}'", id)).FirstOrDefault();
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
            
            return new Team();
        }

        public void Insert(ITeam item)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        cn.Execute(("INSERT TeamTable (Id, Name)" +
                                    "VALUES (@Id, @Name)"), item);
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Insert(IEnumerable<ITeam> teams)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        cn.Execute(("INSERT TeamTable (Id, Name)" +
                                    "VALUES (@Id, @Name)"), teams);
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Update(ITeam item)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        var updateQuery = string.Format("UPDATE TeamTable " +
                                                        "SET Id = '" + item.Id + "', Name = '" + item.Name + "'" +
                                                        "WHERE Id = '" + item.Id + "'");
                        cn.Execute(updateQuery);
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Update(IEnumerable<ITeam> teams)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        foreach (var item in teams)
                        {
                            var updateQuery = string.Format("UPDATE TeamTable " +
                                                            "SET Id = '" + item.Id + "', Name = '" + item.Name + "'" +
                                                            "WHERE Id = '" + item.Id + "'");
                            cn.Execute(updateQuery);
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Delete(Guid id)
        {
            try
            {
                using (var cn = new SqlConnection(_connectionString))
                {
                    cn.Open();
                    {
                        cn.Execute(string.Format("DELETE FROM TeamTable" + 
                                                 "WHERE Id = '{0}'", id));
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
