﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.ViewModels.Interfaces
{
    public interface ITeamsViewModel : IViewModel
    {
        IEnumerable<ITeam> Teams { get; set; }
    }
}
