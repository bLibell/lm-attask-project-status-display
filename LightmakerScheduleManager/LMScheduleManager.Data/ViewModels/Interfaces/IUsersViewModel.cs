﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.ViewModels.Interfaces
{
    public interface IUsersViewModel : IViewModel
    {
        IEnumerable<IUser> Users { get; set; }
        ITeam Team { get; set; }
    }
}
