﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.ViewModels.Interfaces
{
    public interface IViewModel
    {
        string SessionId { get; set; }
    }
}
