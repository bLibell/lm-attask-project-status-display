﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMScheduleManager.Models.DataModels.Interfaces;

namespace LMScheduleManager.Models.ViewModels.Interfaces
{
    public interface IIndexViewModel : IViewModel
    {
        IEnumerable<IScheduleEntry> ScheduleForWeek { get; set; }
        DateTime StartDay { get; }
        DateTime EndDay { get; }
    }
}
