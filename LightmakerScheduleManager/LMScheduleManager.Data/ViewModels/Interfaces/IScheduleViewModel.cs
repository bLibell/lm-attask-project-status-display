﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.ViewModels.Interfaces
{
    public interface IScheduleViewModel : IViewModel
    {
        IEnumerable<ITask> Tasks { get; set; }
        IEnumerable<IScheduleEntry> WeeklySchedule { get; set; }
        IUser User { get; set; }
        DateTime StartDay { get; }
        DateTime EndDay { get; }
    }
}
