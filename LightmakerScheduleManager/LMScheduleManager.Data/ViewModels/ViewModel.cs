﻿using LMScheduleManager.Models.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.ViewModels
{
    public class ViewModel : IViewModel
    {
        public string SessionId { get; set; }
    }
}
