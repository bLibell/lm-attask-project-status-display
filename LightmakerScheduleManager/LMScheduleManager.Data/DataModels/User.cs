﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.DataModels
{
    public class User : IUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ITeam Team { get; set; }
    }
}
