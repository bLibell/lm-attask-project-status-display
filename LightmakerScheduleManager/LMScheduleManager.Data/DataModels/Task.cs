﻿using LMScheduleManager.Models.DataModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.DataModels
{
    public class Task : ITask
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IProject Project { get; set; }
        public DateTime DueDate { get; set; }
        public int TotalHours { get; set; }
        public int UsedHours { get; set; }
        public IEnumerable<IUser> AssignedUsers { get; set; }
    }
}
