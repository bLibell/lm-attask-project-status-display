﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.DataModels.Interfaces
{
    public interface IScheduleEntry
    {
        Guid Id { get; set; }
        DateTime Date { get; set; }
        string Abbreviation { get; set; }
        int Hours { get; set; }
        IUser User { get; set; }
        ITask Task { get; set; }
    }
}
