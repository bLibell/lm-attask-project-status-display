﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.DataModels.Interfaces
{
    public interface ITask
    {
        Guid Id { get; set; }
        string Name { get; set; }
        IProject Project { get; set; }
        DateTime DueDate { get; set; }
        int TotalHours { get; set; }
        int UsedHours { get; set; }
        IEnumerable<IUser> AssignedUsers { get; set; }

    }
}
