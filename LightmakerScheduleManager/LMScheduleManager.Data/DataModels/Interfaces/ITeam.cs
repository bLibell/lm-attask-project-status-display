﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.DataModels.Interfaces
{
    public interface ITeam
    {
        Guid Id { get; set; }
        string Name { get; set; }
    }
}
