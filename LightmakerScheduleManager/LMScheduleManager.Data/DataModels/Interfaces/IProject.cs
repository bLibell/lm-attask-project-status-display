﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMScheduleManager.Models.DataModels.Interfaces
{
    public interface IProject
    {
        Guid Id { get; set; }
        string Name { get; set; }

    }
}
