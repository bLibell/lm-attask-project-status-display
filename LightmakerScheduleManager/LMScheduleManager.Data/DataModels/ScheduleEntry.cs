﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMScheduleManager.Models.DataModels.Interfaces;

namespace LMScheduleManager.Models.DataModels
{
    public class ScheduleEntry : IScheduleEntry
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Abbreviation { get; set; }
        public int Hours { get; set; }
        public IUser User { get; set; }
        public ITask Task { get; set; }
    }
}
